#
#  Copyright (c) 2015-2016 Texas Instruments Incorporated - http://www.ti.com
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#  *  Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#
#  *  Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#
#  *  Neither the name of Texas Instruments Incorporated nor the names of
#     its contributors may be used to endorse or promote products derived
#     from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
#  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
#  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
#  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
#  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

#
#  ======== makefile ========
#

#### Provide toolchain and dependency paths when building manually ####
#DEPOT = C:/ti

#### BIOS-side dependencies ####
#BIOS_INSTALL_PATH       ?= $(DEPOT)/bios_n_nn_nn_nn
#XDC_INSTALL_PATH        ?= $(DEPOT)/xdctools_n_nn_nn_nn_core
#PDK_INSTALL_PATH        ?= $(DEPOT)/pdk_platform_n_n_n/packages

#### BIOS-side toolchains ####
#TOOLCHAIN_PATH_A15   ?= $(DEPOT)/ccsv6/tools/compiler/gcc-arm-none-eabi-n_n-xxxxqn
#TOOLCHAIN_PATH_M4    ?= $(DEPOT)/ccsv6/tools/compiler/ti-cgt-arm_n.n.n
#C6X_GEN_INSTALL_PATH ?= $(DEPOT)/ccsv6/tools/compiler/c6000_n.n.nn
#TOOLCHAIN_PATH_M4    ?= $(DEPOT)/ccsv6/tools/compiler/ti-cgt-arm_n.n.n
#TOOLCHAIN_PATH_A15   ?= $(DEPOT)/ccsv6/tools/compiler/gcc-arm-none-eabi-n_n-20xxqn
#TOOLCHAIN_PATH_A9    ?= $(DEPOT)/ccsv6/tools/compiler/gcc-arm-none-eabi-n_n-20xxqn
#TOOLCHAIN_PATH_A8    ?= $(DEPOT)/ccsv6/tools/compiler/gcc-arm-none-eabi-n_n-20xxqn

ifeq ($(RULES_MAKE), )
include $(PDK_INSTALL_PATH)/ti/build/Rules.make
else
include $(RULES_MAKE)
endif

srcs = dhry_1.c dhry_2.c
objs = $(addprefix bin/$(SOC)/$(CORE)/$(PROFILE)/obj/,$(patsubst %.c,%.out,$(srcs)))
libs =
CONFIG = bin/$(SOC)/$(CORE)/$(PROFILE)/configuro

-include $(addprefix bin/$(SOC)/$(CORE)/$(PROFILE)/obj/,$(patsubst %.c,%.out.dep,$(srcs)))

.PRECIOUS: %/compiler.opt %/linker.cmd
.PHONY: prune

all:
#	$(MAKE) PROFILE=debug SOCDEF=SOC_AM574x CORE=armv7 SOC=AM574x dhry.x
#	$(MAKE) PROFILE=debug SOCDEF=SOC_AM574x CORE=m4    SOC=AM574x dhry.x
#	$(MAKE) PROFILE=debug SOCDEF=SOC_AM574x CORE=c66   SOC=AM574x dhry.x
#	$(MAKE) PROFILE=debug SOCDEF=SOC_AM335x CORE=armv7 SOC=AM335x dhry.x
#	$(MAKE) PROFILE=debug SOCDEF=SOC_AM437x CORE=armv7 SOC=AM437x dhry.x
#	$(MAKE) PROFILE=debug SOCDEF=SOC_AM572x CORE=armv7 SOC=AM572x dhry.x
#	$(MAKE) PROFILE=debug SOCDEF=SOC_AM572x CORE=m4    SOC=AM572x dhry.x
#	$(MAKE) PROFILE=debug SOCDEF=SOC_AM572x CORE=c66   SOC=AM572x dhry.x
#	$(MAKE) PROFILE=debug SOCDEF=DEVICE_KEYSTONE CORE=armv7 SOC=k2e dhry.x
#	$(MAKE) PROFILE=debug SOCDEF=DEVICE_KEYSTONE CORE=c66   SOC=k2e dhry.x
#	$(MAKE) PROFILE=debug SOCDEF=DEVICE_KEYSTONE CORE=armv7 SOC=k2l dhry.x
#	$(MAKE) PROFILE=debug SOCDEF=DEVICE_KEYSTONE CORE=c66   SOC=k2l dhry.x
#	$(MAKE) PROFILE=debug SOCDEF=DEVICE_KEYSTONE CORE=armv7 SOC=k2g dhry.x
#	$(MAKE) PROFILE=debug SOCDEF=DEVICE_KEYSTONE CORE=c66   SOC=k2g dhry.x
#	$(MAKE) PROFILE=debug SOCDEF=DEVICE_KEYSTONE CORE=armv7 SOC=k2h dhry.x
#	$(MAKE) PROFILE=debug SOCDEF=DEVICE_KEYSTONE CORE=c66   SOC=k2h dhry.x
#	$(MAKE) PROFILE=debug SOCDEF=SOC_C6657  CORE=c66   SOC=C6657 dhry.x
#	$(MAKE) PROFILE=debug SOCDEF=SOC_C6678  CORE=c66   SOC=C6678 dhry.x

DEMO_LOC = posix-smp
ifeq ($(SOC),AM572x)
    ifeq ($(CORE),armv7)
        RTSC_PLAT = evmAM572X
        RTSC_TARG = gnu.targets.arm.A15F
        TOOLCHAIN_PATH = $(TOOLCHAIN_PATH_A15)
        ROV = dhry_pa15fg.rov.xs
    else 
        ifeq ($(CORE),c66)
            RTSC_PLAT = evmAM572X
            RTSC_TARG = ti.targets.elf.C66
            TOOLCHAIN_PATH = $(C6X_GEN_INSTALL_PATH)
            ROV = dhry_pe66.rov.xs
        else
            ifeq ($(CORE),m4)
                RTSC_PLAT = evmAM572X
                RTSC_TARG = ti.targets.arm.elf.M4
                TOOLCHAIN_PATH = $(TOOLCHAIN_PATH_M4)
                ROV = dhry_pem4.rov.xs
            endif
        endif
    endif
else
    ifeq ($(SOC),k2e)
        ifeq ($(CORE),armv7)
            RTSC_PLAT = evmC66AK2E:host
            RTSC_TARG = gnu.targets.arm.A15F
            TOOLCHAIN_PATH = $(TOOLCHAIN_PATH_A15)
            ROV = dhry_pa15fg.rov.xs
        else 
            ifeq ($(CORE),c66)
                RTSC_PLAT = evmC66AK2E
                RTSC_TARG = ti.targets.elf.C66
                TOOLCHAIN_PATH = $(C6X_GEN_INSTALL_PATH)
                ROV = dhry_pe66.rov.xs
            endif
        endif
    else
        ifeq ($(SOC),k2h)
            ifeq ($(CORE),armv7)
                RTSC_PLAT = evmTCI6636K2H:host
                RTSC_TARG = gnu.targets.arm.A15F
                TOOLCHAIN_PATH = $(TOOLCHAIN_PATH_A15)
                ROV = dhry_pa15fg.rov.xs
            else
                ifeq ($(CORE),c66)
                RTSC_PLAT = evmTCI6636K2H:core
                RTSC_TARG = ti.targets.elf.C66
                TOOLCHAIN_PATH = $(C6X_GEN_INSTALL_PATH)
                ROV = dhry_pe66.rov.xs
                endif
            endif
        else
            ifeq ($(SOC),k2g)
                ifeq ($(CORE),armv7)
                    RTSC_PLAT = evmTCI66AK2G02:host
                    RTSC_TARG = gnu.targets.arm.A15F
                    TOOLCHAIN_PATH = $(TOOLCHAIN_PATH_A15)
                    ROV = dhry_pa15fg.rov.xs
                else
                    ifeq ($(CORE),c66)
                    RTSC_PLAT = evmTCI66AK2G02:core
                    RTSC_TARG = ti.targets.elf.C66
                    TOOLCHAIN_PATH = $(C6X_GEN_INSTALL_PATH)
                    ROV = dhry_pe66.rov.xs
                    endif
                endif
            else
                ifeq ($(SOC),k2l)
                    ifeq ($(CORE),armv7)
                        RTSC_PLAT = evmTCI6630K2L:host
                        RTSC_TARG = gnu.targets.arm.A15F
                        TOOLCHAIN_PATH = $(TOOLCHAIN_PATH_A15)
                        ROV = dhry_pa15fg.rov.xs
                    else
                        ifeq ($(CORE),c66)
                        RTSC_PLAT = evmTCI6630K2L:core
                        RTSC_TARG = ti.targets.elf.C66
                        TOOLCHAIN_PATH = $(C6X_GEN_INSTALL_PATH)
                        ROV = dhry_pe66.rov.xs
                        endif
                    endif
                else
                    ifeq ($(SOC),C6657)
                        ifeq ($(CORE),c66)
                        RTSC_PLAT = evm6657
                        RTSC_TARG = ti.targets.elf.C66
                        TOOLCHAIN_PATH = $(C6X_GEN_INSTALL_PATH)
                        ROV = dhry_pe66.rov.xs
                        DEMO_LOC = posix
                        endif
                    else
                        ifeq ($(SOC),C6678)
                            ifeq ($(CORE),c66)
                            RTSC_PLAT = evm6678
                            RTSC_TARG = ti.targets.elf.C66
                            TOOLCHAIN_PATH = $(C6X_GEN_INSTALL_PATH)
                            ROV = dhry_pe66.rov.xs
                            DEMO_LOC = posix
                            endif
                        else
                            ifeq ($(SOC),AM335x)
                                ifeq ($(CORE),armv7)
                                RTSC_PLAT = evmAM3359
                                RTSC_TARG = gnu.targets.arm.A8F
                                TOOLCHAIN_PATH = $(TOOLCHAIN_PATH_A8)
                                ROV = dhry_pa8fg.rov.xs
                                endif
                            else
                                ifeq ($(SOC),AM437x)
                                    ifeq ($(CORE),armv7)
                                    RTSC_PLAT = evmAM437X
                                    RTSC_TARG = gnu.targets.arm.A9F
                                    TOOLCHAIN_PATH = $(TOOLCHAIN_PATH_A9)
                                    ROV = dhry_pa9fg.rov.xs
                                    endif
                                else
                                    ifeq ($(SOC),AM574x)
                                        ifeq ($(CORE),armv7)
                                            RTSC_PLAT = idkAM572X
                                            RTSC_TARG = gnu.targets.arm.A15F
                                            TOOLCHAIN_PATH = $(TOOLCHAIN_PATH_A15)
                                            ROV = dhry_pa15fg.rov.xs
                                        else
                                            ifeq ($(CORE),c66)
                                                RTSC_PLAT = idkAM572X
                                                RTSC_TARG = ti.targets.elf.C66
                                                TOOLCHAIN_PATH = $(C6X_GEN_INSTALL_PATH)
                                                ROV = dhry_pe66.rov.xs
                                            else
                                                ifeq ($(CORE),m4)
                                                    RTSC_PLAT = idkAM572X
                                                    RTSC_TARG = ti.targets.arm.elf.M4
                                                    TOOLCHAIN_PATH = $(TOOLCHAIN_PATH_M4)
                                                    ROV = dhry_pem4.rov.xs
                                                endif
                                            endif
                                        endif
                                    endif
                                endif
                            endif
                        endif
                    endif
                endif
            endif
        endif
    endif
endif
#add/remove prune to the below line to clean configuro and obj
dhry.x: bin/$(SOC)/$(CORE)/$(PROFILE)/dhry.out
bin/$(SOC)/$(CORE)/$(PROFILE)/dhry.out: $(objs) $(libs) $(CONFIG)/linker.cmd
	@$(ECHO) "#"
	@$(ECHO) "# Making $@ ..."
	$(LD) $(LDFLAGS) $(objs) $(libs) $(SUFFIX)

bin/$(SOC)/$(CORE)/$(PROFILE)/obj/%.out: %.c $(CONFIG)/compiler.opt
	@$(ECHO) "#"
	@$(ECHO) "# Making $@ ..."
	$(CC) -O3 -D$(SOCDEF) -D$(SOC) $(CPPFLAGS) $(CFLAGS)

%/compiler.opt: %/linker.cmd ;
%/linker.cmd: $(SOC)/$(CORE)/bios/dhry.cfg
	echo $(TOOLCHAIN_PATH_M4)
	@$(ECHO) "#"
	@$(ECHO) "# Making $@ ..."
	$(XDC_INSTALL_PATH)/xs --xdcpath="$(subst +,;,$(PKGPATH))" \
            xdc.tools.configuro -o $(CONFIG) \
            -t $(RTSC_TARG) \
            -c $(TOOLCHAIN_PATH) \
            -p ti.platforms.$(RTSC_PLAT) \
            -r debug \
            --cfgArgs "{ \
                profile: \"$(PROFILE)\" \
            }" $(SOC)/$(CORE)/bios/dhry.cfg

install:
	install -d $(INSTALL_BASE_DIR)/demos/$(DEMO_LOC)
	install -d $(INSTALL_BASE_DIR)/demos/$(DEMO_LOC)/bin/$(SOC)/$(CORE)/$(PROFILE)
	install -d $(INSTALL_BASE_DIR)/demos/$(DEMO_LOC)/$(SOC)/$(CORE)/bios
	install -c -m 755 $(EXEC_DIR)/bin/$(SOC)/$(CORE)/$(PROFILE)/dhry.out \
        $(INSTALL_BASE_DIR)/demos/$(DEMO_LOC)/bin/$(SOC)/$(CORE)/$(PROFILE)/dhry.out
	install -c -m 755 $(EXEC_DIR)/$(SOC)/$(CORE)/bios/dhry.cfg $(INSTALL_BASE_DIR)/demos/$(DEMO_LOC)/$(SOC)/$(CORE)/bios/dhry.cfg
	$(CP) -r $(EXEC_DIR)/*.c $(INSTALL_BASE_DIR)/demos/$(DEMO_LOC)/
	$(CP) -r $(EXEC_DIR)/*.h $(INSTALL_BASE_DIR)/demos/$(DEMO_LOC)/
	$(CP) -r $(EXEC_DIR)/*.txt $(INSTALL_BASE_DIR)/demos/$(DEMO_LOC)/
	$(CP) -r $(EXEC_DIR)/makefile $(INSTALL_BASE_DIR)/demos/$(DEMO_LOC)/

help:
	@$(ECHO) "make                   # build executable"
	@$(ECHO) "make clean             # clean everything"

clean:
	$(RMDIR) bin

prune:
	$(CP) bin/$(SOC)/$(CORE)/$(PROFILE)/configuro/package/cfg/$(ROV) \
        bin/$(SOC)/$(CORE)/$(PROFILE)/$(ROV)
	$(RMDIR) bin/$(SOC)/$(CORE)/$(PROFILE)/configuro
	$(RMDIR) bin/$(SOC)/$(CORE)/$(PROFILE)/obj

PKGPATH := $(BIOS_INSTALL_PATH)/packages
PKGPATH := $(PKGPATH)+$(XDC_INSTALL_PATH)/packages+$(PDK_INSTALL_PATH)

#  ======== install validation ========
ifeq (install,$(MAKECMDGOALS))
ifeq (,$(EXEC_DIR))
$(error must specify EXEC_DIR)
endif
endif

#  ======== toolchain macros ========
ifeq ($(CORE),armv7)
    CC = $(TOOLCHAIN_PATH)/bin/arm-none-eabi-gcc -c -MD -MF $@.dep
    LD = $(TOOLCHAIN_PATH)/bin/arm-none-eabi-gcc -o $@
    SUFFIX = -Wl,-T,$(CONFIG)/linker.cmd \
		-lgcc -lc -lm -lrdimon $(LDLIBS)
else
    ifeq ($(CORE),m4)
        CC = $(TOOLCHAIN_PATH)/bin/armcl -c -qq -ppd=$@.dep -ppa
        LD = $(TOOLCHAIN_PATH)/bin/armcl
        SUFFIX = -o "$@"
    else
        CC = $(TOOLCHAIN_PATH)/bin/cl6x -c -qq -pdsw225 -ppd=$@.dep -ppa
        LD = $(TOOLCHAIN_PATH)/bin/cl6x
        SUFFIX = -o "$@"
    endif
endif

CPPFLAGS = -Dfar= -D__DYNAMIC_REENT__ -DBIOS_POSIX 
ifeq ($(CORE),armv7)
    CFLAGS = -std=c99 -I$(BIOS_INSTALL_PATH)/packages/ti/posix/gcc/ -Wall -Wunused -Wunknown-pragmas -ffunction-sections -fdata-sections $(CCPROFILE_$(PROFILE)) @$(CONFIG)/compiler.opt -I. -o $@ $<
    LDFLAGS = $(LDPROFILE_$(PROFILE)) -mfloat-abi=hard -nostartfiles -Wl,-static -Wl,--gc-sections -Wl,-Map=$(@D)/obj/$(@F).map
    LDLIBS = -L$(BIOS_INSTALL_PATH)/packages/gnu/targets/arm/libs/install-native/arm-none-eabi/lib/hard --specs=nano.specs
else
    ifeq ($(CORE),m4)
        CFLAGS = -I$(BIOS_INSTALL_PATH)/packages/ti/posix/ccs/ $(CCPROFILE_$(PROFILE)) -@"$(CONFIG)/compiler.opt" --output_file=$@ -fc $<
        LDFLAGS = $(LDPROFILE_$(PROFILE)) -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -g -z -m$(@D)/obj/$(@F).map --heap_size=0x800 --stack_size=0x800 -i"$(TOOLCHAIN_PATH_M4)/lib" --reread_libs --display_error_number --warn_sections --rom_model --strict_compatibility=on -l"$(CONFIG)/linker.cmd" -l"libc.a" 
        LDLIBS =
    else
        CFLAGS = -I$(BIOS_INSTALL_PATH)/packages/ti/posix/ccs/ $(CCPROFILE_$(PROFILE))  --mem_model:const=far --mem_model:data=far_aggregates -@"$(CONFIG)/compiler.opt" --output_file=$@ -fc $<
        #LDFLAGS = $(LDPROFILE_$(PROFILE)) -w -q -c -m $(@D)/obj/$(@F).map
        LDFLAGS = $(LDPROFILE_$(PROFILE)) --abi=eabi -g -z -m$(@D)/obj/$(@F).map --heap_size=0x800 --stack_size=0x800 -i"$(C6X_GEN_INSTALL_PATH)/lib" --reread_libs --display_error_number --warn_sections --rom_model --strict_compatibility=on -l"$(CONFIG)/linker.cmd" -l"libc.a"
        LDLIBS = -l $(CGTOOLS)/lib/rts6600_elf.lib
    endif
endif

ifeq ($(CORE),armv7)
    CCPROFILE_debug = -g -ggdb -D_DEBUG_=1
else
    ifeq ($(CORE),m4)
        CPROFILE_debug = -D_DEBUG_=1 --symdebug:dwarf
    else
        CPROFILE_debug = -D_DEBUG_=1 --symdebug:dwarf
    endif
endif
CCPROFILE_release = -O2

LDPROFILE_debug = -g -ggdb
LDPROFILE_release =

#  ======== standard macros ========
ifneq (,$(wildcard $(XDC_INSTALL_PATH)/bin/echo.exe))
    # use these on Windows
    CP      = $(XDC_INSTALL_PATH)/bin/cp
    ECHO    = $(XDC_INSTALL_PATH)/bin/echo
    MKDIR   = $(XDC_INSTALL_PATH)/bin/mkdir -p
    RM      = $(XDC_INSTALL_PATH)/bin/rm -f
    RMDIR   = $(XDC_INSTALL_PATH)/bin/rm -rf
else
    # use these on Linux
    CP      = cp
    ECHO    = echo
    MKDIR   = mkdir -p
    RM      = rm -f
    RMDIR   = rm -rf
endif

#  ======== create output directories ========
ifneq (clean,$(MAKECMDGOALS))
ifneq (,$(PROFILE))
ifeq (,$(wildcard bin/$(SOC)/$(CORE)/$(PROFILE)/obj))
    $(shell $(MKDIR) -p bin/$(SOC)/$(CORE)/$(PROFILE)/obj)
endif
endif
endif
