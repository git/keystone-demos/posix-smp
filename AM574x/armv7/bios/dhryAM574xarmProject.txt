-ccs.linkFile "PDK_INSTALL_PATH/ti/utils/dhry/dhry.h"
-ccs.linkFile "PDK_INSTALL_PATH/ti/utils/dhry/dhry_1.c"
-ccs.linkFile "PDK_INSTALL_PATH/ti/utils/dhry/dhry_2.c"
-ccs.linkFile "PDK_INSTALL_PATH/ti/utils/dhry/AM574x/armv7/bios/dhry.cfg"
-ccs.setCompilerOptions "-c -mfloat-abi=hard -DBIOS_POSIX -DREG=register -g -gstrict-dwarf -MMD -MP"
-rtsc.enableRtsc
-ccs.setLinkerOptions " -lgcc -lm -lrdimon -nostartfiles -static -Wl,--gc-sections -L$(BIOS_INSTALL_PATH)/packages/gnu/targets/arm/libs/install-native/arm-none-eabi/lib/hard --specs=nano.specs"
